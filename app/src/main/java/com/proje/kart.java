package com.proje;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatDrawableManager;
import android.widget.Button;

/**
 * Created by makina on 8.03.2018.
 */

public class kart extends Button {

    boolean acikMi=false;
    boolean cevrilebilir =true;

    int arkaPlanID,onPlanID=0;

    Drawable arka,on;

    public kart(Context context, int id,int alan) { //Bir default constructor oluşturduk.
        super(context);


       setId(id);

        if(alan==12) {
            arkaPlanID=R.drawable.logo;
            if (id % 6 == 1)
                onPlanID = R.drawable.c1;
            if (id % 6 == 2)
                onPlanID = R.drawable.c2;
            if (id % 6 == 3)
                onPlanID = R.drawable.c3;
            if (id % 6 == 4)
                onPlanID = R.drawable.c4;
            if (id % 6 == 5)
                onPlanID = R.drawable.c5;
            if (id % 6 == 0)
                onPlanID = R.drawable.c6;

        }

        if(alan==16) {
            arkaPlanID=R.drawable.logo;
            if (id % 8 == 1)
                onPlanID = R.drawable.c1;
            if (id % 8 == 2)
                onPlanID = R.drawable.c2;
            if (id % 8 == 3)
                onPlanID = R.drawable.c3;
            if (id % 8 == 4)
                onPlanID = R.drawable.c4;
            if (id % 8 == 5)
                onPlanID = R.drawable.c5;
            if (id % 8 == 6)
                onPlanID = R.drawable.c6;
            if (id % 8 == 7)
                onPlanID = R.drawable.c7;
            if (id % 8 == 0)
                onPlanID = R.drawable.c8;
        }

        if(alan==20) {
            arkaPlanID=R.drawable.logo;
            if (id % 10 == 1)
                onPlanID = R.drawable.c1;
            if (id % 10 == 2)
                onPlanID = R.drawable.c2;
            if (id % 10 == 3)
                onPlanID = R.drawable.c3;
            if (id % 10 == 4)
                onPlanID = R.drawable.c4;
            if (id % 10 == 5)
                onPlanID = R.drawable.c5;
            if (id % 10 == 6)
                onPlanID = R.drawable.c6;
            if (id % 10 == 7)
                onPlanID = R.drawable.c7;
            if (id % 10 == 8)
                onPlanID = R.drawable.c8;
            if (id % 10 == 9)
                onPlanID = R.drawable.c9;
            if (id % 10 == 0)
                onPlanID = R.drawable.c10;
        }

        if(alan==36){
            arkaPlanID=R.drawable.logo6;
            if (id % 18 == 1)
                onPlanID = R.drawable.e1;
            if (id % 18 == 2)
                onPlanID = R.drawable.e2;
            if (id % 18 == 3)
                onPlanID = R.drawable.e3;
            if (id % 18== 4)
                onPlanID = R.drawable.e4;
            if (id % 18 == 5)
                onPlanID = R.drawable.e5;
            if (id % 18 == 6)
                onPlanID = R.drawable.e6;
            if (id % 18 == 7)
                onPlanID = R.drawable.e7;
            if (id % 18== 8)
                onPlanID = R.drawable.e8;
            if (id % 18 == 9)
                onPlanID = R.drawable.e9;
            if (id % 18 == 10)
                onPlanID = R.drawable.e10;
            if (id % 18 == 11)
                onPlanID = R.drawable.e11;
            if (id % 18 == 12)
                onPlanID = R.drawable.e12;
            if (id % 18 == 13)
                onPlanID = R.drawable.e13;
            if (id % 18 == 14)
                onPlanID = R.drawable.e14;
            if (id % 18 == 15)
                onPlanID = R.drawable.e15;
            if (id % 18 == 16)
                onPlanID = R.drawable.e16;
            if (id % 18 == 17)
                onPlanID = R.drawable.e17;
            if (id % 18 == 0)
                onPlanID = R.drawable.e18;
        }
        arka = AppCompatDrawableManager.get().getDrawable(context,arkaPlanID);
        on = AppCompatDrawableManager.get().getDrawable(context,onPlanID);
        setBackground(arka);
    }

    public void cevir(){
        if(cevrilebilir) {
            if (acikMi == false) {
                setBackground(on);
                acikMi = true;
            } else {
                setBackground(arka);
                acikMi = false;
            }
        }
    }
}
