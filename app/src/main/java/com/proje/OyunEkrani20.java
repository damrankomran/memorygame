package com.proje;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Chronometer;
import android.widget.GridLayout;
import android.widget.TextView;

public class OyunEkrani20 extends AppCompatActivity {

    int sonKart=0,skor=0,hata=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oyun_ekrani20);

        final Chronometer chronometer = (Chronometer)findViewById(R.id.chronometer4);
        chronometer.start();

        Intent i =getIntent();                          //Intent oluşturup AnaEkran'daki text'i alıcaz
        final String s= i.getStringExtra("mesaj");
        final int alan = (i.getIntExtra("alan",0));             //BolumSec'den gelen alan sayısını aldık

        TextView tv = (TextView)findViewById(R.id.textView6); //TextView'da aldıgımız mesajı görüntüledik.
        tv.setText("Hoşgeldin "+s);

        GridLayout gl = (GridLayout)findViewById(R.id.kartlar20);

        kart kartlar[]= new kart[alan];
        for(int j=1;j<=alan;j++){
            kartlar[j-1] = new kart(this,j,alan);

            kartlar[j-1].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final kart k = (kart)view;//view a kart dönüşümü yaptık
                    k.cevir();
                    if(sonKart>0){
                        final kart k2 = (kart)findViewById(sonKart);

                        if(k2.onPlanID==k.onPlanID && k2.getId() != k.getId()){
                            //eşleştirler
                            k2.cevrilebilir=false;
                            k.cevrilebilir=false;
                            skor++;
                            sonKart=0;
                            TextView tv = (TextView)findViewById(R.id.skor20);
                            tv.setText("Skor: "+skor);

                            if(skor==alan/2){
                                chronometer.stop();
                                Intent i = new Intent(OyunEkrani20.this,SkorTablosu.class);
                                String finishTime = chronometer.getText().toString();
                                i.putExtra("finishTime",finishTime);
                                i.putExtra("hata",hata);
                                i.putExtra("isim",s);
                                i.putExtra("alan",20);

                                startActivity(i);
                            }
                        }
                        else{
                            //eşleşmediler geri cevir

                            Handler h = new Handler();
                            h.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    k2.cevir();
                                    k.cevir();
                                }
                            },500);
                            hata++;
                            sonKart=0;

                            TextView tv =(TextView)findViewById(R.id.hata20);
                            tv.setText("Hata: "+hata);
                        }
                    }
                    else{
                        sonKart=k.getId();
                    }
                }
            });


        }

        //Karistir
        for(int j=0; j<alan;j++){
            int rg = (int)(Math.random()*alan);
            kart k = kartlar[rg];
            kartlar[rg]=kartlar[j];
            kartlar[j]=k;
        }



        //Goruntule
        for(int j=0;j<alan;j++){
            gl.addView(kartlar[j]);
        }

    }
}
