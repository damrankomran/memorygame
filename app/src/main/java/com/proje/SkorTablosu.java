package com.proje;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SkorTablosu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skor_tablosu);


        Intent i = getIntent();

        int hata = i.getIntExtra("hata", 0);
        final String isim = i.getStringExtra("isim");
        final String finishTime = i.getStringExtra("finishTime");
        final int alan = i.getIntExtra("alan", 0);
        TextView tv = (TextView) findViewById(R.id.oyunsonu);
        tv.setText("Harika!! " + isim + " oyunu " + hata + " hata ile " + finishTime + " sürede bitirdin!!");

        ((Button) findViewById(R.id.yenidenoyna)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (alan == 16) {
                    Intent i = new Intent(SkorTablosu.this, OyunEkrani.class);
                    i.putExtra("mesaj", isim);
                    i.putExtra("alan", alan);
                    startActivity(i);
                }
                if (alan == 12) {
                    Intent i = new Intent(SkorTablosu.this, OyunEkrani12.class);
                    i.putExtra("mesaj", isim);
                    i.putExtra("alan", alan);
                    startActivity(i);
                }
                if (alan == 20) {
                    Intent i = new Intent(SkorTablosu.this, OyunEkrani20.class);
                    i.putExtra("mesaj", isim);
                    i.putExtra("alan", alan);
                    startActivity(i);
                }

                if (alan == 36) {
                    Intent i = new Intent(SkorTablosu.this, OyunEkrani6.class);
                    i.putExtra("mesaj", isim);
                    i.putExtra("alan", alan);
                    startActivity(i);
                }
            }
        });

    }
}
