package com.proje;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class BolumSec extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bolum_sec);

        Intent i =getIntent();
        final String isim = i.getStringExtra("mesaj");

        ((Button)findViewById(R.id.onikilik)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(BolumSec.this,OyunEkrani.class);
                i.putExtra("mesaj",isim);
                i.putExtra("alan",12);
                i.putExtra("row",4);
                i.putExtra("column",3);
                startActivity(i);

            }
        });

        ((Button)findViewById(R.id.onaltılık)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(BolumSec.this,OyunEkrani.class);
                i.putExtra("mesaj",isim);
                i.putExtra("alan",16);
                i.putExtra("row",4);
                i.putExtra("column",4);
                startActivity(i);
            }
        });
        ((Button)findViewById(R.id.yirmilik)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(BolumSec.this,OyunEkrani.class);
                i.putExtra("mesaj",isim);
                i.putExtra("alan",20);
                i.putExtra("row",5);
                i.putExtra("column",4);
                startActivity(i);
            }
        });
        ((Button)findViewById(R.id.otuzaltılık)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(BolumSec.this,OyunEkrani.class);
                i.putExtra("mesaj",isim);
                i.putExtra("alan",36);
                i.putExtra("row",9);
                i.putExtra("column",4);
                startActivity(i);
            }
        });



    }

}
