package com.proje;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AnaEkran extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ana_ekran);
        final EditText et = (EditText)findViewById(R.id.editText); //EditText'teki metnin degismemesi için final yaptık


        ((Button)findViewById(R.id.button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(AnaEkran.this,BolumSec.class); //anaEkran'dan Oyun ekranına geçiş için intent oluşturduk
                i.putExtra("mesaj",et.getText().toString());           //putExtra fonk'u ile mesajı yolladık.
                startActivity(i);
            }
        });
    }
}
