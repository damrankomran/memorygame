package com.proje.model;

/**
 * Created by makina on 31.03.2018.
 */

public class Card {

    private Integer id;
    private boolean opened;
    private boolean turnable;

    public Card(Integer id, boolean opened, boolean turnable) {
        this.id = id;
        this.opened = opened;
        this.turnable = turnable;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isOpened() {
        return opened;
    }

    public void setOpened(boolean opened) {
        this.opened = opened;
    }

    public boolean isTurnable() {
        return turnable;
    }

    public void setTurnable(boolean turnable) {
        this.turnable = turnable;
    }
}
